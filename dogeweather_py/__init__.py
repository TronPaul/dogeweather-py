import os
from flask import Flask, request, abort, render_template, url_for
from flask_s3 import FlaskS3, create_all
import requests

IP_LOCATION_API_URL = 'http://ip-api.com/json/'
WEATHER_API_URL = 'http://api.openweathermap.org/data/2.5/weather'

app = Flask(__name__)
app.config['WEATHER_APP_ID'] = os.environ.get('WEATHER_APP_ID', None)
app.config['FLASKS3_BUCKET_NAME'] = os.environ.get('FLASKS3_BUCKET_NAME', None)
app.debug = os.environ.get('DEBUG', None) == '1'
if app.config['FLASKS3_BUCKET_NAME']:
    s3 = FlaskS3(app)


def get_request_location():
    if 'X-Forwarded-For' in request.headers:
        ip = request.headers.getlist("X-Forwarded-For")[0].rpartition(' ')[-1]
    elif request.remote_addr:
        ip = request.remote_addr
    else:
        return None
    resp = requests.get(IP_LOCATION_API_URL + ip)
    try:
        resp.raise_for_status()
        rv = resp.json()
        return rv['lat'], rv['lon']
    except:
        return None


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/weather')
def weather():
    location = request.args.get('location')
    lat = request.args.get('lat')
    lon = request.args.get('lon')

    if not location and not (lat and lon):
        lat, lon = get_request_location() or 41.8781, 87.6298  # Chicago #1

    args = {'APPID': app.config['WEATHER_APP_ID']}
    if lat and lon:
        args['lat'] = lat
        args['lon'] = lon
    elif location:
        args['q'] = location
    if len(args) <= 1:
        abort(400)
    resp = requests.get(WEATHER_API_URL, args)
    try:
        resp.raise_for_status()
        return resp.content, 200
    except:
        abort(500)


# Flask-S3 isn't exactly what we want here for a mostly static site
def deploy_static_assets(zappa):
    app.config['FLASKS3_BUCKET_NAME'] = zappa.zappa_settings[zappa.api_stage]['environment_variables']['FLASKS3_BUCKET_NAME']
    FlaskS3(app)
    create_all(app)


if __name__ == '__main__':
    if 'WEATHER_APP_ID' not in app.config:
        raise RuntimeError()
    app.run()
